# Fine Tuning Question Answering Model

## Table of Contents

- [Overview](#introduction)
- [Setup](#dependencies)
- [Training](#run-training)
- [Inference](#run-inference)

## <a name="introduction"></a>Project Overview
This project is dedicated to fine-tuning a machine learning model using a specific dataset. It encompasses the following steps:

- **Data Loading**: The project will load the dataset from Hugging Face repo.
- **Data Preprocessing**: It includes preprocessing steps to prepare the data for model training.
- **Model Training**: The code for training the machine learning model is included.
- **Model Testing**: The code for evaluating the performance of the trained model is also provided.

## <a name="dependencies"></a>Setup
```
pip install transformers
pip install datasets
huggingface-cli login
```

## <a name="run-training"></a>Run Training

```
python -u path\to\main.py
```
When running this :
- You will be asked to enter dataset path (SQuAD format is required)
    - if it's left empty => the default value will be my dataset ***Ryan20/qa_hotel_dataset_2*** will be loaded
- Load dataset
- Preprocess the data
- Train the model
- Push to hub

<!-- ![main run image](./Screenshots/main_run.png) 
-->
<p align="center">
  <img src="./Screenshots/main_run.png" alt="main run image" height="400">
</p>
- The default dataset here is ***Ryan20/qa_hotel_dataset_2*** : https://huggingface.co/datasets/Ryan20/qa_hotel_dataset_2/tree/main
- Your given dataset should be in SQuAD format :

<!-- ![SQuAD Dataset image](./Screenshots/SQuAD_dataset.png) -->
<p align="center">
  <img src="./Screenshots/SQuAD_dataset.png" alt="SQuAD Dataset image" height="400">
</p>


## <a name="run-inference"></a>Run Inference

```
python -u path\to\pipeline_inference.py
```
When running this :
- You will be asked to enter a question
- The output will show :
    - **Question**
    - **Answer**
    - **Starting Index of answer**

<!-- ![inference run image](./Screenshots/inference_run.png) -->
<p align="center">
  <img src="./Screenshots/inference_run.png" alt="inference run image" height="400">
</p>

Notice : my fine tuned model still doesn't give correct answers, I just made sure the process is running and here I am sharing it with you !