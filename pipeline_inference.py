from flask import Flask, request, render_template
from transformers import pipeline
from constants import context

app = Flask(__name__, template_folder='Frontend')

@app.route("/", methods=["GET", "POST"])
def qa():
    if request.method == "POST":
        question = request.form["question"]
        model_path = request.form["model_path"]

        if not model_path:
            model_path = "nova-sqoin/hotel_qa_model"

        question_answerer = pipeline("question-answering", model=model_path)
        answer = question_answerer(question=question, context=context)["answer"]
        index = question_answerer(question=question, context=context)["start"]

        return render_template("index.html", question=question, answer=answer, index=index)

    return render_template("index.html")

if __name__ == "__main__":
    app.run(debug=True)